## Software required

To import data (h5 or csv), use Motion Studio (https://support.apdm.com/hc/en-us), one of APDM's software products. 
NOTE! As of 2022/08, Motion Studio doesn't require a password to download, while other products from APDM do. 

## Recording data
These sensors are easy to use. From the USB cradle you remove as many as you want to use and they will sync together. 

It's a good idea to label and configure them before use. In Motion Studio, click the 'Configure' button or use 'Tools->Create New Configuration' to bring up an interface.

## Interpreting blinks
The blinking sensors at (~0.5 Hz) are working and will (should) have data logging. If they are blinking together the assumption is that they are synced.  

## Using motion studio
Motion studio is a crappy program and the files are annoying stored (at least on Mac; they are stored within the Application, at 
(/Applications/MotionStudio.app/Contents/Resources/workspace). 

You use 'tools->Convert Logged Data' to bring up an interface.

You select the files you want, click 'Enqueue' and then 'Convert'. Ugh. Enqueue is required. But I guess 'enqueue' is handy if you want to do some files first to get looking at them, while churning through the rest...

You can then export any files or convert to csv. 

## Troubleshooting

Steps to take if an Opal appears unhappy:

Dock/Undock: When you dock and undock an Opal (either in a docking station or a powered USB cable), it will re-read the configuration file, reset the individual sensors (accelerometer, etc.) and reset the radio. This will clear certain types of errors indicated by a red flashing LED, and is the first thing you should try as it is fast and easy.

Soft reset: A soft reset will shut down all of the Opal's systems and bring them back up again to conform to the saved configuration file. To perform this operation: 1) Hold button #2 (the one near the bottom of the display) for 3s until the boot menu comes up, 2) Push button #1 three times to navigate to the "Boot Menu" option, 3) Push button #2 to select this option, 4) Push button #1 once to navigate to the "Reset" option, and 5) Push button #2 to select this option.

Power cycle: Power cycling your opal is identical to a soft reset, with the exception that it completely powers down until you power it on again. It is possible for the Opal to maintain some electric state in the internal capacitors, which typically dissipate over 5-10s. Try this option if a soft reset does not address your issue. To perform this operation: 1) Hold button #2 (the one near the bottom of the display) for 3s until the boot menu comes up, 2) Push button #1 twice to navigate to the "Power Off" option, 3) Push button #2 to select this option. At this point, the Opal will be powered off. Wait 10s and dock it or plug it back into a USB cable to turn it on again.

Reset Configuration: This process resets the configuration file to the defaults, and reconfigures the Opal based on these defaults. It does not shut down your Opals systems and bring them back up again. To perform this operation: 1) Hold button #2 (the one near the bottom of the display) for 3s until the boot menu comes up, 2) Push button #1 three times to navigate to the "Boot Menu" option, 3) Push button #2 to select this option, 4) Push button #1 twice to navigate to the "Reset Configuration" option, and 5) Push button #2 to select this option.

Format SD: This process erases and reformats the internal storage on the Opal. This includes the configuration file (as the option above does) in addition to all logged data. To perform this operation: 1) Hold button #2 (the one near the bottom of the display) for 3s until the boot menu comes up, 2) Push button #1 three times to navigate to the "Boot Menu" option, 3) Push button #2 to select this option, 4) Push button #1 three times to navigate to the "Format SD" option, and 5) Push button #2 to select this option.

Hard Reset: A hard reset is identical to a soft reset, but can usually be performed if the Opal/AP is otherwise unresponsive. To perform this operation: 1) Plug the Opal/AP into a powered USB cable (either connected to your computer or a wall charger), 2) Hold both buttons for 30s until you see the LED come on and the Opal/AP reset.

Force Bootloader Mode: This operation may be necessary if the firmware on your Opal or Access Point gets corrupted due to an incomplete firmware update process. The symptom is that is fails to start up after performing a hard reset. Typically, there will be text that indicates that the firmware should be updated, but other states are possible as well (e.g., a "snowy" screen"). When in this state, it will not be visible by the firmware update process and a special operation is necessary to force it into bootloader mode such that it can be detected during the firmware update process. To perform this operation: 1) Plug the Opal/AP into a powered USB cable (either connected to your computer or a wall charger), 2) Hold both buttons for 30s until you see the LED turn magenta, 3) Keep holding button #1 while repeatedly pressing and releasing button #2 for about 5-10 seconds. The LED will turn red (which doesn't look all that different than magenta) and the Opal/AP should reset into the bootloader mode. When in this mode, the screen will be blank when plugged in, and will display "Entering bootloader..." (or possibly whatever was last displayed on the screen) when unplugged (unless it is an access point, in which it turns off when unplugged -- so don't unplug the access point after getting it into this mode, or you will have to go through this process again). After getting it into the bootloader mode, attempt to update the firmware again using the Tools->"Update Firmware" method.

From this page: https://support.apdm.com/hc/en-us/articles/115001117826-How-do-I-reset-my-Opal-
